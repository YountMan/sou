<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<meta name="keyword" content="">
<meta name="description" content="">
<meta content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0" name="viewport">
<meta content="telephone=no" name="format-detection">
<link rel="shortcut icon" type="image/ico" href="/favicon.ico">
<title>${keyword}-搜索结果</title>
<link rel="stylesheet" href="/css/mobile.css">
<script type="text/javascript" src="/js/jquery-1.12.1.min.js"></script>
<style type="text/css">
	.data-item p, .data-item a {
		margin-top: 5px;
		white-space:normal;
		word-break: break-all; 	/*支持IE，chrome，FF不支持*/
		word-wrap: break-word;	/*支持IE，chrome，FF*/
	}
	.data-item a {
		display: block;
		height: 20px;
		line-height: 20px;
		overflow:hidden;
		text-overflow:ellipsis;
		-o-text-overflow:ellipsis;
		white-space:nowrap;
		width:100%;
		padding-right: 10px;
	}
</style>
</head>
<body style="background: #f1f1f1">
	<div style="width: 100%;">
		<div class="tool-bar" style="text-align: center; padding: 10px 0; ">
			<form id="kwform">
				<div class="con-wrap">
					<input id="keyword" name="keyword" type="text" autocomplete="off" placeholder=""/>
					<div style="width: 82px; position: relative;">
						<button id="searchBtn" class="search-btn" type="button">网盘一下</button>
					</div>
				</div>
			</form>
		</div>
		
		<div class="item-bar" style="margin-top: 80px;">
			<span>搜索结果</span>
		</div>
		<div style="background: #fff; min-height: 300px; border-top: 1px solid #ddd; padding-bottom: 16px;">
			<div id="resultList" style="min-height: 50px;">
				
			</div>
			<div id="loadMore" class="loadMore" style="display: none;">加载更多...</div>
			<div id="loading"><span>...</span></div>
		</div>
		
		<div class="footer" style="background: none; font-size: 14px; color: #676767; text-align: center; margin-bottom: 10px; margin-top: 0;">
			<p class="friend-link">
				<a href="tencent://message/?uin=1225541107&site=&menu=yes" target="_blank">联系站长</a>
				<a href="http://blog.here325.com/words" target="_blank" style="margin-left: 12px;">给我留言</a>
			</p>
			<p class="friend-link">
				<a href="http://blog.here325.com" target="_blank">大雄博客</a>
				<a href="http://www.wolfbe.com" target="_blank" style="margin-left: 12px;">锋宇博客</a>
			</p>
			<p>©2016&nbsp;325搜&nbsp;您的备案号</p>
		</div>
	</div>
</body>

<script type="text/javascript">
	var currentDataPage = 1;
	$(function(){
		var keyword = '${keyword}';
		$("#keyword").focus().val(keyword);	//输入框聚焦
		
		$("#searchBtn").click(function(){
			getPageData(1);
		});
		if(keyword) $("#searchBtn").click();
		
		$("#loadMore").click(function(){
			var nextPage = currentDataPage + 1;
			getPageData(nextPage);
		});
		
		$(window).scroll(function(){
			var scrollTop = $(document).scrollTop();
			if(scrollTop >= 40) {
				$("#fixedDiv").css({
					'position': 'fixed',
					'top': '65px',
					'z-index': 1
				});
			} else {
				$("#fixedDiv").css({'position': ''});
			}
		});
	});
	
	function getPageData(page) {
		currentDataPage = page;
		var keyword = $("#keyword").val();
		$("#loadMore").hide();
		$("#loading").show();
		
		if(page == 1) $("#resultList").html('');
		$.post("/ajaxSearch", {keyword: keyword, page: page, type: "moblie"}, function(data){
			$("#loading").hide();
			if(data.status == 1) {
				var dataMap = data.data;
				var result = dataMap.items;
				if(result.length < 1) {
					$("#resultList").html("<div class='search-tips'>搜不到您要的资料，换个更优雅的姿势搜吧~~:-D</div>");
					return;
				}
				var html = "";
				for(var i=0; i<result.length; i++) {
					html += 
						'<div class="data-item">'+
						'	<a class="title" href="' + result[i].linkUrl + '" target="_blank">' + result[i].title + '</a>'+
						'	<p>' + result[i].content + '</p>'+
						'	<a class="link-url" href="' + result[i].linkUrl + '" target="_blank">' + result[i].formatUrl + '</a>'+
						'</div>';
				}
				
				var pages = dataMap.pages;
				//上一页
				if(pages) {
					var nextPage = dataMap.page>=pages[pages.length-1].label ? dataMap.page : dataMap.page + 1;
					if(nextPage == currentDataPage) {
						$("#loadMore").hide();
					} else {
						$("#loadMore").show();
					}
				} else {
					$("#loadMore").hide();
				}
				
				if(page == 1) $("#resultList").html(html);
				else if (page > 1) $("#resultList").append(html);
			} else {
				$("#resultList").html("<div class='search-tips'>搜不到您要的资料，换个更优雅的姿势搜吧~~:-D</div>");
			}
		});
	}
</script>
<div id="script"></div>
</html>
