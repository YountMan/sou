<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<meta name="keywords" content="">
<meta name="description" content="">
<!-- <link rel="shortcut icon" type="image/ico" href="/favicon.ico"> -->
<title>${keyword}-搜索结果</title>
<link rel="stylesheet" href="/css/imhere.css">
<script type="text/javascript" src="/js/jquery-1.12.1.min.js"></script>
<script type="text/javascript" src="/js/imhere.js"></script>
</head>
<body>
	<div id="container">
		<div class="tool-bar">
			<form id="kwform" action="/index" method="post">
				<div class="small-logo">
					<img src="/img/logo3.png" alt="" title=""/>
				</div>
				<div style="height: 54px; float: left;">
					<div style="position: relative;margin: 10px 0;">
						<input id="keyword" name="keyword" type="text" autocomplete="off" placeholder="请输入关键字"/>
						<ul id="keywordSug" style="top: 34px;"></ul>
					</div>
				</div>
				<div style="height: 54px; float: left;">
					<button id="searchBtn" class="search-btn" type="button">网盘一下</button>
				</div>
			</form>
			
			<div class="link-entrance">
				<a href="http://blog.here325.com" target="_blank">大雄博客</a>&nbsp;&nbsp;
			</div>
		</div>
		
		<div class="nav-bar">
			<div>
				<div class="nav-item">
					搜索结果
				</div>
			</div>
		</div>
		
		<div class="result-zone">
			<div class="result-list">
				<div id="resultList">
				</div>
				
				<div id="loading" class="loading">
					<img alt="加载中" src="/img/loading01.gif">
					<span>正在加载结果，稍安勿躁......</span>
				</div>
			</div>
			
			<div class="result-right">
				<div id="fixedDiv" class="left-block">
					<div class="im_title">
						<h3>支持本站</h3>
					</div>
					<div class="donate-div">
						<div id="switchPic" style="margin: 0 auto;">
							<span id="weixin" class="active">微&nbsp;&nbsp;信</span>
							&nbsp;&nbsp;
							<span id="zhifubao">支付宝</span>
						</div>
						<div class="donate-outer">
							<div class="donate-inner">
								<img class="weixin active" alt="捐赠支持本站" src="/img/weixin.jpg">
								<img class="zhifubao" alt="捐赠支持本站" src="/img/zhifubao.png" style="width: 220px; height: 220px; margin-top: 10px; display: none;">
							</div>
						</div>
						<div style="margin: 0 auto; font-size: 14px; margin-top: 10px; color: #5A7949;">您的捐赠将用于本站的服务器的购买和维护支出</div>
					</div>
					<div class="im_title" style="margin-top: 40px;"><h3>广告展示</h3></div>
					<div style="height: 278px; border: 1px solid #ccc; ">
						<div style="height: 250px; width: 300px; margin: 0 auto; margin-top: 12px;">
						<!-- 这里是搜狗广告的代码 -->
						<script type="text/javascript">
							var sogou_ad_id=570225;
							var sogou_ad_height=250;
							var sogou_ad_width=300;
						</script>
						<!-- <script type='text/javascript' src='http://images.sohu.com/cs/jsfile/js/c.js'></script> -->
						</div>
					</div>
					
				</div>
				<div style="clear: both;"></div>
			</div>
			
			<div style="clear: both;"></div>
		</div>
		
		<div class="footer">
			<p class="friend-link">
               	站长QQ：
               	<a href="tencent://message/?uin=10000&site=&menu=yes" title="点我发起QQ聊天">88888888</a>
               	<a href="tencent://message/?uin=10000&site=&menu=yes" style="margin-left: 5px;" title="点我发起QQ聊天">
                       	<img src="/img/qq.jpg" style="width: 20px; height: 20px; vertical-align: middle;"/>
               	</a>
           	</p>
			<p class="friend-link">
				友情链接：
				<a href="http://blog.here325.com" target="_blank">大雄博客</a>&nbsp;|&nbsp;
				<a href="http://blog.here325.com/words" target="_blank">留言给我</a>&nbsp;|&nbsp;
				<a href="http://www.wolfbe.com" target="_blank">锋宇博客</a>&nbsp;|&nbsp;
				<a href="http://pan.here325.com" target="_blank">325搜</a>
			</p>
			<p>Copy Right ©2016 版权所有，违者必究。 <span class="beian-icon">您的备案号</span></p>
			<p>
				免责声明：搜索结果来自第三方API，本站不产生/存储任何数据，搜索结果不代表本站任何观点。如有侵权，联系站长删除。
			</p>
		</div>
	</div>
</body>

<script type="text/javascript">
	$(function(){
		$("#keyword").focus().val('${keyword}');	//输入框聚焦
		$("#searchBtn").click(function(){
			getPageData(1);
		});
		
		$("#searchBtn").click();
		
		$(window).keydown(function(e){
			var keyCode = e.which || e.keyCode;
			if(keyCode==116) {	//刷新按钮
				$("#kwform").submit();
				e.preventDefault();
				return;
			}
		});
		
		$("#switchPic span").click(function(){
			var isActive = $(this).hasClass("active");
			if(isActive) return;
			$("#switchPic span").removeClass("active");
			$(this).addClass("active");
			$(".donate-inner img").hide();
			var domId = $(this).attr("id");
			$(".donate-inner ." + domId).show();
		});
	});
	
	function getPageData(page) {
		var keyword = $("#keyword").val();
		$("#loading").show();

		page = page || 1;
		$.post("/ajaxSearch", {keyword: keyword, page: page, type: "pc"}, function(data){
			$("#loading").hide();
			if(data.status == 1) {
				var dataMap = data.data;
				var result = dataMap.items;
				if(result.length < 1) {
					$("#resultList").html("<div class='search-tips'>搜不到您要的资料，换个更优雅的姿势搜吧~~:-D</div>");
					return;
				}
				var html = "";

				for(var i=0; i<result.length; i++) {
					html += '<div class="data-item">'+
							'	<a class="title" href="' + result[i].linkUrl + '" target="_blank">' + result[i].title + '</a>'+
                            '       <p>' + result[i].content + '</p>'+
                            '	<a class="link-url" href="' + result[i].linkUrl + '" target="_blank">' + result[i].formatUrl + '</a>'+
                            '</div>';
				}
				
				var pages = dataMap.pages;
				//上一页
				var prePage = dataMap.page > 1 ? dataMap.page-1 : 1;
				html += 
					'<div class="page-info">' +
					'	<ul class="page-ul">' +
					'		<li onclick="getPageData('+prePage+');">上一页</li>';
				
				for(var i=0; i<pages.length; i++) {
					var clz = dataMap.page==pages[i].label ? ' class="current" ' : '';
					html += '<li' + clz + ' onclick="getPageData(' + pages[i].label + ');">' + pages[i].label + '</li>';
				}
				
				var nextPage = dataMap.page>=pages[pages.length-1].label ? dataMap.page : dataMap.page + 1;
				html += 
				'		<li onclick="getPageData('+nextPage+');">下一页</li>' +
				'	</ul>' +
				'</div>';
				
				$("#resultList").html(html);
			} else {
				$("#resultList").html("<div class='search-tips'>"+data.msg+"</div>");
			}
		});
	}
</script>
<div id="script"></div>
<script>window._bd_share_config={"common":{"bdSnsKey":{},"bdText":"","bdMini":"2","bdMiniList":["mshare","qzone","weixin","tsina","sqq","tqq","tieba","renren","douban","bdysc","bdxc","tqf","youdao","mail"],"bdPic":"","bdStyle":"0","bdSize":"16"},"slide":{"type":"slide","bdImg":"1","bdPos":"right","bdTop":"150"}};with(document)0[(getElementsByTagName('head')[0]||body).appendChild(createElement('script')).src='http://bdimg.share.baidu.com/static/api/js/share.js?v=89860593.js?cdnversion='+~(-new Date()/36e5)];</script>
</html>